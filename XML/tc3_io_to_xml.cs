// $Header: https://redoubt.ligo-wa.caltech.edu/svn/slowcontrols/trunk/Scripts3/Common/read_links_from_csv.cs 3138 2016-09-07 04:46:29Z patrick.thomas@LIGO.ORG $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

using System.Linq;

public class TreeItem {
	public TreeItem() {
		this.PreviousPort = new PreviousPort();
		this.TxPdo_Index_List = new System.Collections.Generic.List<string>();
		this.RxPdo_Index_List = new System.Collections.Generic.List<string>();
		this.InitCmd_List = new System.Collections.Generic.List<string>();
	}

	public string Name {get; set;}
	public string Comment {get; set;}
	public int Disabled {get; set;}
	public string PathName {get; set;}
	public int ItemType {get; set;}
	public int ItemSubType {get; set;}
	public string ProductRevision {get; set;}
	public string PhysAddr {get; set;}
	public string SyncUnit {get; set;}
	public PreviousPort PreviousPort {get; set;}
	public System.Collections.Generic.List<string> TxPdo_Index_List {get; set;}
	public System.Collections.Generic.List<string> RxPdo_Index_List {get; set;}
	public System.Collections.Generic.List<string> InitCmd_List {get; set;}
}

public class PreviousPort {
	public string Port {get; set;}
	public string PhysAddr {get; set;}
	public string PathName {get; set;}
}


class Program {
	static System.Collections.Generic.List<TreeItem> tree_item_list = new System.Collections.Generic.List<TreeItem>();


	[System.STAThread]
	static void Main(string[] args) {
		string solution_file_path = args[0];
		string xml_file_path = args[1];


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Open(solution_file_path);

			dynamic project = solution.Projects.Item(1);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			TCatSysManagerLib.ITcSmTreeItem tiid = system_manager.LookupTreeItem("TIID");

			scan_tree(tiid);

			System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<TreeItem>), new System.Type[] {typeof(TreeItem), typeof(PreviousPort)});
			System.IO.TextWriter writer = new System.IO.StreamWriter(xml_file_path);
			x.Serialize(writer, tree_item_list);
			writer.Close();
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}


	static System.Collections.Generic.Dictionary<string, string> phys_addr_to_path_name_dict = new System.Collections.Generic.Dictionary<string, string>();

	static void scan_tree(TCatSysManagerLib.ITcSmTreeItem parent) {
		foreach (TCatSysManagerLib.ITcSmTreeItem child in parent) {
			TreeItem tree_item = new TreeItem();

			if (child.ItemType == 2 || child.ItemType == 5 || child.ItemType == 6) {
				System.Console.WriteLine(child.Name);

				tree_item.Name = child.Name;
				tree_item.Comment = child.Comment;
				tree_item.Disabled = (int) child.Disabled;
				tree_item.PathName = child.PathName;
				tree_item.ItemType = child.ItemType;
				tree_item.ItemSubType = child.ItemSubType;

				System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
				doc.LoadXml(child.ProduceXml());

				System.Xml.XmlNode node;

				node = doc.SelectSingleNode(@"/TreeItem/EtherCAT/Slave/Info/ProductRevision");
				if (node != null) {
					tree_item.ProductRevision = node.InnerText.Trim();
				}

				node = doc.SelectSingleNode(@"/TreeItem/EtherCAT/Slave/Info/PhysAddr");
				if (node != null) {
					tree_item.PhysAddr = node.InnerText.Trim();
					phys_addr_to_path_name_dict[tree_item.PhysAddr] = tree_item.PathName;
				}

				node = doc.SelectSingleNode(@"/TreeItem/EtherCAT/Slave/SyncUnits/SyncUnit");
				if (node != null) {
					tree_item.SyncUnit = node.InnerText.Trim();
				}

				node = doc.SelectSingleNode(@"/TreeItem/EtherCAT/Slave/PreviousPort[@Selected='true']");
				if (node != null) {
					System.Xml.XmlNode sub_node;

					sub_node = node.SelectSingleNode(@"Port");
					if (sub_node != null) {
						tree_item.PreviousPort.Port = sub_node.InnerText.Trim();
					}

					sub_node = node.SelectSingleNode(@"PhysAddr");
					if (sub_node != null) {
						tree_item.PreviousPort.PhysAddr = sub_node.InnerText.Trim();
						tree_item.PreviousPort.PathName = phys_addr_to_path_name_dict[tree_item.PreviousPort.PhysAddr];
					}
				}
				else {
					tree_item.PreviousPort.PathName = parent.PathName;
				}


				System.Xml.XmlNodeList node_list;


				node_list = doc.SelectNodes(@"/TreeItem/EtherCAT/Slave/ProcessData/TxPdo[@Sm='3']");
				foreach (System.Xml.XmlNode sub_node in node_list) {
					string index = sub_node.SelectSingleNode(@"Index").InnerText.Trim();
					tree_item.TxPdo_Index_List.Add(index);
				}

				node_list = doc.SelectNodes(@"/TreeItem/EtherCAT/Slave/ProcessData/RxPdo[@Sm='2']");
				foreach (System.Xml.XmlNode sub_node in node_list) {
					string index = sub_node.SelectSingleNode(@"Index").InnerText.Trim();
					tree_item.RxPdo_Index_List.Add(index);
				}


				node_list = doc.SelectNodes(@"/TreeItem/EtherCAT/Slave/Mailbox/CoE/InitCmds/InitCmd[not(@Fixed)]");
				foreach (System.Xml.XmlNode sub_node in node_list) {
					string xml = @"<TreeItem><EtherCAT><Slave><Mailbox><CoE><InitCmds>" + sub_node.OuterXml + @"</InitCmds></CoE></Mailbox></Slave></EtherCAT></TreeItem>";
					tree_item.InitCmd_List.Add(xml);
				}

				tree_item_list.Add(tree_item);

				scan_tree(child);
			}
		}
	}
}

