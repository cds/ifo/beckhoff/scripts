// $Header: https://redoubt.ligo-wa.caltech.edu/svn/slowcontrols/trunk/TwinCAT3/Vacuum/LHO/Source/h0vacex_create_target.cs 3649 2018-01-22 19:17:46Z patrick.thomas@LIGO.ORG $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

// Test of the creation and configuration of IO terminals.

using System;
using System.Linq;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Text.RegularExpressions;
using TCatSysManagerLib;


public class TreeItem {
	public TreeItem() {
		this.PhysAddr = "";
		this.SyncUnit = "";
		this.PreviousPort = new PreviousPort();
		this.TxPdo_Index_List = new System.Collections.Generic.List<string>();
		this.RxPdo_Index_List = new System.Collections.Generic.List<string>();
		this.InitCmd_List = new System.Collections.Generic.List<string>();
	}

	public string Name {get; set;}
	public string Comment {get; set;}
	public int Disabled {get; set;}
	public string PathName {get; set;}
	public int ItemType {get; set;}
	public int ItemSubType {get; set;}
	public string ProductRevision {get; set;}
	public string PhysAddr {get; set;}
	public string SyncUnit {get; set;}
	public PreviousPort PreviousPort {get; set;}
	public System.Collections.Generic.List<string> TxPdo_Index_List {get; set;}
	public System.Collections.Generic.List<string> RxPdo_Index_List {get; set;}
	public System.Collections.Generic.List<string> InitCmd_List {get; set;}
}

public class PreviousPort {
	public PreviousPort() {
		this.Port = "";
		this.PhysAddr = "";
		this.PathName = "";
	}

	public string Port {get; set;}
	public string PhysAddr {get; set;}
	public string PathName {get; set;}
}


class Program {
	static void set_Disabled(TCatSysManagerLib.ITcSmTreeItem item) {
		item.Disabled = DISABLED_STATE.SMDS_DISABLED;
	}

	static void set_PreviousPort(TCatSysManagerLib.ITcSmTreeItem item, string Port, string PhysAddr) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><PreviousPort Selected=""true""><Port>{0}</Port><PhysAddr>{1}</PhysAddr></PreviousPort></Slave></EtherCAT></TreeItem>", Port, PhysAddr);
		item.ConsumeXml(xml);
	}

	static void set_SyncUnit(TCatSysManagerLib.ITcSmTreeItem item, string SyncUnit) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>{0}</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>", SyncUnit);
		item.ConsumeXml(xml);
	}

	static void set_PhysAddr(TCatSysManagerLib.ITcSmTreeItem item, string PhysAddr) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Info><PhysAddr>{0}</PhysAddr><PhysAddrFixed>true</PhysAddrFixed></Info></Slave></EtherCAT></TreeItem>", PhysAddr);
		item.ConsumeXml(xml);
	}

	static void set_InitCmd(TCatSysManagerLib.ITcSmTreeItem item, string Transition, string Comment, string Timeout, string Ccs, string Index, string SubIndex, string Data) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Mailbox><CoE><InitCmds><InitCmd><Transition>{0}</Transition><Comment><![CDATA[{1}]]></Comment><Timeout>{2}</Timeout><Ccs>{3}</Ccs><Index>{4}</Index><SubIndex>{5}</SubIndex><Data>{6}</Data></InitCmd></InitCmds></CoE></Mailbox></Slave></EtherCAT></TreeItem>", Transition, Comment, Timeout, Ccs, Index, SubIndex, Data);
		item.ConsumeXml(xml);
	}

	static void set_ProcessData(TCatSysManagerLib.ITcSmTreeItem item, System.Collections.Generic.List<string> pdo_input_list, System.Collections.Generic.List<string> pdo_output_list) {
		System.Xml.XmlDocument item_xml = new System.Xml.XmlDocument();
		item_xml.LoadXml(item.ProduceXml());

		// Removes all of the Sm attributes for the outputs.
		System.Console.WriteLine("Removing PDO outputs");
		System.Xml.XmlNodeList rxpdo_node_list = item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo");
		foreach (System.Xml.XmlElement rxpdo_element in rxpdo_node_list) {
			rxpdo_element.RemoveAttribute("Sm");
		}

		// Removes all of the Sm attributes for the inputs.
		System.Console.WriteLine("Removing PDO inputs");
		System.Xml.XmlNodeList txpdo_node_list = item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo");
		foreach (System.Xml.XmlElement txpdo_element in txpdo_node_list) {
			txpdo_element.RemoveAttribute("Sm");
		}

		// Sets the Sm attribute for the outputs.
		System.Console.WriteLine("Setting PDO outputs");
		foreach (string pdo_output in pdo_output_list) {
			System.Console.WriteLine(pdo_output);
			string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo[Index = ""{0}""]", pdo_output);
			((System.Xml.XmlElement) item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "2");
		}

		// Sets the Sm attribute for the inputs.
		System.Console.WriteLine("Setting PDO inputs");
		foreach (string pdo_input in pdo_input_list) {
			System.Console.WriteLine(pdo_input);
			string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo[Index = ""{0}""]", pdo_input);
			((System.Xml.XmlElement) item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "3");
		}

		item.ConsumeXml(item_xml.OuterXml);
	}


	[System.STAThread]
	static void Main(string[] args) {
		//string solution_file_path = args[0];
		string xml_file_path = args[0];

		string solution_name = "TESTH1ECATC1";
		string solution_folder_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1";
		string solution_file_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1\TESTH1ECATC1.sln";

		string project_name = "TESTH1ECATC1";
		string project_folder_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1\TESTH1ECATC1";


		// Deletes the folder at the solution folder path if it exists.
		if (System.IO.Directory.Exists(solution_folder_path)) {
			System.IO.Directory.Delete(solution_folder_path, true);
		}

		// Deletes the folder at the project folder path if it exists.
		if (System.IO.Directory.Exists(project_folder_path)) {
			System.IO.Directory.Delete(project_folder_path, true);
		}

		// Creates folders at the solution and folder paths.
		System.IO.Directory.CreateDirectory(solution_folder_path);
		System.IO.Directory.CreateDirectory(project_folder_path);


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Create(solution_folder_path, solution_name);
			solution.SaveAs(solution_file_path);

			string template_path = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj";
			dynamic project = solution.AddFromTemplate(template_path, project_folder_path, project_name);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(System.Collections.Generic.List<TreeItem>), new System.Type[] {typeof(TreeItem), typeof(PreviousPort)});
			System.IO.StreamReader reader = new System.IO.StreamReader(xml_file_path);
			System.Collections.Generic.List<TreeItem> tree_item_list = (System.Collections.Generic.List<TreeItem>) x.Deserialize(reader);
			reader.Close();

			foreach (TreeItem tree_item in tree_item_list) {
				TCatSysManagerLib.ITcSmTreeItem parent;
				TCatSysManagerLib.ITcSmTreeItem child;

				System.Console.WriteLine(tree_item.Name);

				parent = system_manager.LookupTreeItem(tree_item.PreviousPort.PathName);
				child = parent.CreateChild(tree_item.Name, tree_item.ItemSubType, "", tree_item.ProductRevision);

				if (tree_item.Disabled == 0 && tree_item.PhysAddr.Length != 0) {
					set_PhysAddr(child, tree_item.PhysAddr);
				}

				if (tree_item.PreviousPort.Port.Length != 0 && tree_item.PreviousPort.PhysAddr.Length != 0) {
					set_PreviousPort(child, tree_item.PreviousPort.Port, tree_item.PreviousPort.PhysAddr);
				}

				if (tree_item.SyncUnit.Length != 0) {
					set_SyncUnit(child, tree_item.SyncUnit);
				}

				if (tree_item.TxPdo_Index_List.Count != 0 || tree_item.RxPdo_Index_List.Count != 0) {
					set_ProcessData(child, tree_item.TxPdo_Index_List, tree_item.RxPdo_Index_List);
				}

				foreach (string xml in tree_item.InitCmd_List) {
					child.ConsumeXml(xml);
				}

				if (tree_item.Disabled == 1) {
					set_Disabled(child);
				}
			}


			// Saves the project.
			project.Save();


			// Saves the solution.
			// solution.Save() does not exist.
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}
}


