// $Header: $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

using System.Linq;

class Program {
	[System.STAThread]
	static int Main(string[] args) {
/*
		if (args.Length != 2) {
			System.Console.WriteLine("Invalid argument count");
			return -1;
		}
*/

		//string altium_xml_file_path = System.IO.Path.GetFullPath(args[0]);
		//string solution_folder_path = System.IO.Path.GetFullPath(args[1]);
		string altium_xml_file_path = @"C:\SlowControls\TwinCAT3\Source\Interferometer\H1EcatC1\Configure\H1EcatC1_BoxList.xml";
		string solution_folder_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\H1EcatC1\h1ecatc1";

		string target_namespace = @"https://dcc.ligo.org/LIGO-E1800220/public";
		string schema_uri = @"C:\SlowControls\Drawings\Documents\TcAltium.xsd";

		System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

		System.Xml.Schema.XmlSchemaSet xml_schema_set = new System.Xml.Schema.XmlSchemaSet();
		xml_schema_set.Add(target_namespace, schema_uri);

		System.Xml.XmlReaderSettings xml_reader_settings = new System.Xml.XmlReaderSettings();
		xml_reader_settings.Schemas.Add(xml_schema_set);
		xml_reader_settings.ValidationFlags |= System.Xml.Schema.XmlSchemaValidationFlags.ReportValidationWarnings;
		xml_reader_settings.ValidationType = System.Xml.ValidationType.Schema;

		System.Xml.XmlReader xml_reader = System.Xml.XmlReader.Create(altium_xml_file_path, xml_reader_settings);

		System.Xml.XmlNamespaceManager namespace_manager = new System.Xml.XmlNamespaceManager(xml_reader.NameTable);
		namespace_manager.AddNamespace("x", target_namespace);

		try {
			doc.Load(xml_reader);
		}
		catch (System.Xml.Schema.XmlSchemaValidationException e) {
			System.Console.WriteLine(e.Message);
			return -1;
		}

		xml_reader.Close();


		string base_path = doc.SelectSingleNode(@"x:TcAltium/x:System/x:BasePath", namespace_manager).InnerText.Trim();
		string solution_name = doc.SelectSingleNode(@"x:TcAltium/x:System/x:Name", namespace_manager).InnerText.Trim();
		string project_name = solution_name;

		// Deletes the solution folder if it already exists.
		if (System.IO.Directory.Exists(solution_folder_path)) {
			try {
				System.IO.Directory.Delete(solution_folder_path, true);
				System.Threading.Thread.Sleep(2000);
			}
			catch (System.IO.IOException e) {
				System.Console.WriteLine(e.Message);
			}
		}

		System.IO.Directory.CreateDirectory(solution_folder_path);


		string project_folder_path = solution_folder_path;


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2) obj;

		try {
			dte.MainWindow.Activate();


			// Creates the solution
			dynamic solution = dte.Solution;
			solution.Create(solution_folder_path, solution_name);


			// Saves the solution
			string solution_file_path = System.IO.Path.ChangeExtension(System.IO.Path.Combine(solution_folder_path, solution_name), ".sln");
			solution.SaveAs(solution_file_path);


			// Creates the project
			string template_path = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj";
			dynamic project = solution.AddFromTemplate(template_path, project_folder_path, project_name);


			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			TCatSysManagerLib.ITcSmTreeItem tirs = system_manager.LookupTreeItem("TIRS");
			TCatSysManagerLib.ITcSmTreeItem tirt = system_manager.LookupTreeItem("TIRT");
			TCatSysManagerLib.ITcSmTreeItem tipc = system_manager.LookupTreeItem("TIPC");
			TCatSysManagerLib.ITcSmTreeItem tiid = system_manager.LookupTreeItem("TIID");


			// RealTime

			// The RTimeSetDef node is not required by the schema
			System.Xml.XmlNode realtime_rtimesetdef_node = doc.SelectSingleNode("x:TcAltium/x:RealTime/x:RTimeSetDef", namespace_manager);
			if (realtime_rtimesetdef_node != null) {
				System.Console.WriteLine("RealTime");

				string realtime_rtimesetdef_outerxml = realtime_rtimesetdef_node.OuterXml;
				System.Console.WriteLine(realtime_rtimesetdef_outerxml);

				string consume_xml = "<TreeItem>" + realtime_rtimesetdef_outerxml + "</TreeItem>";

				// Throws an exception only upon malformed xml
				tirs.ConsumeXml(consume_xml);

				System.Console.WriteLine("");
			}


			// Tasks

			System.Console.WriteLine("Tasks");

			// At least one task is required by the schema
			System.Xml.XmlNodeList tasks_task_node_list = doc.SelectNodes(@"x:TcAltium/x:Tasks/x:Task", namespace_manager);
			foreach (System.Xml.XmlNode tasks_task_node in tasks_task_node_list) {
				// The 'Name' node is required by the schema
				string tasks_task_name = tasks_task_node.SelectSingleNode(@"x:Name", namespace_manager).InnerText.Trim();
				System.Console.WriteLine(tasks_task_name);

				TCatSysManagerLib.ITcSmTreeItem task_tree_item;
				if (tasks_task_name == "I/O Idle Task") {
					// Throws an exception upon failure
					task_tree_item = tirs.LookupChild(tasks_task_name);
				}
				else {
					// Creates the task
					task_tree_item = tirt.CreateChild(tasks_task_name, 1, null, null);
				}

				// The 'TaskDef' node is required by the schema
				string tasks_task_taskdef_outerxml = tasks_task_node.SelectSingleNode(@"x:TaskDef", namespace_manager).OuterXml;
				System.Console.WriteLine(tasks_task_taskdef_outerxml);

				string consume_xml = "<TreeItem>" + tasks_task_taskdef_outerxml + "</TreeItem>";

				// Throws an exception only upon malformed xml
				task_tree_item.ConsumeXml(consume_xml);

				System.Console.WriteLine("");
			}


			// PLCs

			System.Xml.XmlNodeList plcs_plc_node_list = doc.SelectNodes(@"x:TcAltium/x:PLCs/x:PLC", namespace_manager);

			if (plcs_plc_node_list.Count != 0) {
				System.Console.WriteLine("PLCs");
			}

			foreach (System.Xml.XmlNode plcs_plc_node in plcs_plc_node_list) {
				// The 'Name' node is required by the schema
				string plcs_plc_name = plcs_plc_node.SelectSingleNode(@"x:Name", namespace_manager).InnerText.Trim();
				System.Console.WriteLine(plcs_plc_name);

				// The 'Directory' node is required by the schema
				string plcs_plc_directory = plcs_plc_node.SelectSingleNode(@"x:Directory", namespace_manager).InnerText.Trim();
				System.Console.WriteLine(plcs_plc_directory);

				// PLC instance

				// At least one 'Instance' node is required by the schema
				// Only uses the first defined instance
				// It is not clear if it is possible to add more than one instance to a PLC using the automation
				// The 'Filename' node is required by the schema
				string plcs_plc_instances_instance_filename = plcs_plc_node.SelectSingleNode(@"x:Instances/x:Instance/x:Filename", namespace_manager).InnerText.Trim();
				System.Console.WriteLine(plcs_plc_instances_instance_filename);

				string tmc_file_path = System.IO.Path.Combine(base_path, plcs_plc_directory, plcs_plc_instances_instance_filename);
				System.Console.WriteLine(tmc_file_path);

				// Creates the PLC
				// Throws an exception if the tmc_file_path is not found
				tipc.CreateChild(plcs_plc_name, 1, "", tmc_file_path);

				System.Console.WriteLine("");
			}


			// Modules

			System.Xml.XmlNodeList modules_module_node_list = doc.SelectNodes(@"x:TcAltium/x:Modules/x:Module", namespace_manager);

			if (modules_module_node_list.Count != 0) {
				System.Console.WriteLine("Modules");
			}

			foreach (System.Xml.XmlNode modules_module_node in modules_module_node_list) {
				// The 'Name' node is required by the schema
				string modules_module_name = modules_module_node.SelectSingleNode(@"x:Name", namespace_manager).InnerText.Trim();
				System.Console.WriteLine(modules_module_name);

				// Creates the device
				TCatSysManagerLib.ITcSmTreeItem device_tree_item = tiid.CreateChild(modules_module_name, 111, null, null);

				// The 'Device' node is not required by the schema
				System.Xml.XmlNode modules_module_device_node = modules_module_node.SelectSingleNode(@"x:Device", namespace_manager);
				if (modules_module_device_node != null) {
					// To be determined if the description node is required by the schema
					string modules_module_device_description = modules_module_device_node.SelectSingleNode(@"x:Description", namespace_manager).InnerText.Trim();
					System.Console.WriteLine(modules_module_device_description);

					string consume_xml = System.String.Format(@"<TreeItem><DeviceDef><AddressInfo><Pnp><DeviceDesc>{0}</DeviceDesc></Pnp></AddressInfo></DeviceDef></TreeItem>", modules_module_device_description);

					// Throws an exception only on malformed xml
					device_tree_item.ConsumeXml(consume_xml);
				}

				System.Console.WriteLine("");

				System.Xml.XmlNodeList modules_module_modules_module_node_list = modules_module_node.SelectNodes(@".//x:Module", namespace_manager);
				foreach (System.Xml.XmlNode modules_module_modules_module_node in modules_module_modules_module_node_list) {
					// The 'Name' node is required by the schema
					string modules_module_modules_module_name = modules_module_modules_module_node.SelectSingleNode(@"x:Name", namespace_manager).InnerText.Trim();
					System.Console.WriteLine(modules_module_modules_module_name);

					// The 'Part' node is required by the schema
					string modules_module_modules_module_part = modules_module_modules_module_node.SelectSingleNode(@"x:Part", namespace_manager).InnerText.Trim();
					System.Console.WriteLine(modules_module_modules_module_part);

					// The 'Type' node is required by the schema
					string modules_module_modules_module_type = modules_module_modules_module_node.SelectSingleNode(@"x:Type", namespace_manager).InnerText.Trim();
					System.Console.WriteLine(modules_module_modules_module_type);

					// The 'Subtype' node is required by the schema
					int modules_module_modules_module_subtype = System.Int32.Parse(modules_module_modules_module_node.SelectSingleNode(@"x:Subtype", namespace_manager).InnerText.Trim());
					System.Console.WriteLine(modules_module_modules_module_subtype);

					// The 'Parent' node is required by the schema
					string modules_module_modules_module_parent = modules_module_modules_module_node.SelectSingleNode(@"x:Parent", namespace_manager).InnerText.Trim();
					System.Console.WriteLine(modules_module_modules_module_parent);

					TCatSysManagerLib.ITcSmTreeItem parent_tree_item = system_manager.LookupTreeItem(modules_module_modules_module_parent);

					System.Xml.XmlNode modules_module_modules_module_previous_node = modules_module_modules_module_node.SelectSingleNode(@"x:Previous", namespace_manager);

					if (modules_module_modules_module_part == "CU1128-0001") {
						// Changes the name from the autogenerated name

						string hub2_autogenerated_name = modules_module_modules_module_parent.Split('^').Last() + "_2 (CU1128-B)";

						// Throws an exception upon failure
						TCatSysManagerLib.ITcSmTreeItem hub2_tree_item = parent_tree_item.LookupChild(hub2_autogenerated_name);

						string hub2_consume_xml = System.String.Format(@"<TreeItem><ItemName>{0}</ItemName><PathName>{1}</PathName></TreeItem>", modules_module_modules_module_name, modules_module_modules_module_parent + "^" + modules_module_modules_module_name);

						// Throws an exception only on malformed xml
						hub2_tree_item.ConsumeXml(hub2_consume_xml);
					}
					else if (modules_module_modules_module_part == "CU1128-0002") {
						// Changes the name from the autogenerated name

						string hub3_autogenerated_name = modules_module_modules_module_parent.Split('^').Last() + "_3 (CU1128-C)";

						// Throws an exception upon failure
						TCatSysManagerLib.ITcSmTreeItem hub3_tree_item = parent_tree_item.LookupChild(hub3_autogenerated_name);

						string hub3_consume_xml = System.String.Format(@"<TreeItem><ItemName>{0}</ItemName><PathName>{1}</PathName></TreeItem>", modules_module_modules_module_name, modules_module_modules_module_parent + "^" + modules_module_modules_module_name);

						// Throws an exception only on malformed xml
						hub3_tree_item.ConsumeXml(hub3_consume_xml);
					}
					else if (modules_module_modules_module_previous_node != null) {
						// The 'Path' node is required by the schema
						string modules_module_modules_module_previous_path = modules_module_modules_module_previous_node.SelectSingleNode(@"x:Path", namespace_manager).InnerText.Trim();
						System.Console.WriteLine(modules_module_modules_module_previous_path);

						TCatSysManagerLib.ITcSmTreeItem previous_tree_item = system_manager.LookupTreeItem(modules_module_modules_module_previous_path);

						// Creates the device
						TCatSysManagerLib.ITcSmTreeItem current_tree_item = previous_tree_item.CreateChild(modules_module_modules_module_name, modules_module_modules_module_subtype, null, modules_module_modules_module_part);


						string consume_xml;


						// Previous

						// The 'Port' node is required by the schema
						string modules_module_modules_module_previous_port = modules_module_modules_module_previous_node.SelectSingleNode(@"x:Port", namespace_manager).InnerText.Trim();

						System.Console.WriteLine(modules_module_modules_module_previous_port);

						// The 'Addr' node is required by the schema
						string modules_module_modules_module_previous_addr = modules_module_modules_module_previous_node.SelectSingleNode(@"x:Addr", namespace_manager).InnerText.Trim();
						System.Console.WriteLine(modules_module_modules_module_previous_addr);

						consume_xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><PreviousPort Selected=""1""><Port>{0}</Port><PhysAddr>{1}</PhysAddr></PreviousPort></Slave></EtherCAT></TreeItem>", modules_module_modules_module_previous_port, modules_module_modules_module_previous_addr);

						// Throws an exception only on malformed xml
						current_tree_item.ConsumeXml(consume_xml);


						// ECatAddr

						// The 'ECatAddr' node is not required by the schema
						System.Xml.XmlNode modules_module_modules_module_ecataddr_node = modules_module_modules_module_node.SelectSingleNode(@"x:ECatAddr", namespace_manager);
						if (modules_module_modules_module_ecataddr_node != null) {
							string modules_module_modules_module_ecataddr = modules_module_modules_module_ecataddr_node.InnerText.Trim();
							System.Console.WriteLine(modules_module_modules_module_ecataddr);

							consume_xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Info><PhysAddr>{0}</PhysAddr><PhysAddrFixed>true</PhysAddrFixed></Info></Slave></EtherCAT></TreeItem>", modules_module_modules_module_ecataddr);
							current_tree_item.ConsumeXml(consume_xml);
						}


						// Sync Unit

						// The 'SyncUnit' node is not required by the schema
						System.Xml.XmlNode modules_module_modules_module_syncunit_node = modules_module_modules_module_node.SelectSingleNode(@"x:SyncUnit", namespace_manager);
						if (modules_module_modules_module_syncunit_node != null) {
							System.Console.WriteLine("SyncUnit");

							string modules_module_modules_module_syncunit = modules_module_modules_module_syncunit_node.InnerText.Trim();
							System.Console.WriteLine(modules_module_modules_module_syncunit);

							consume_xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>{0}</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>", modules_module_modules_module_syncunit);

							// Throws an exception only on malformed xml
							current_tree_item.ConsumeXml(consume_xml);
						}


						// PDO

						// The 'PDO' node is not required by the schema
						System.Xml.XmlNode modules_module_modules_module_pdo_node = modules_module_modules_module_node.SelectSingleNode(@"x:PDO", namespace_manager);
						if (modules_module_modules_module_pdo_node != null) {
							System.Console.WriteLine("PDO");

							System.Xml.XmlDocument current_tree_item_xml = new System.Xml.XmlDocument();
							current_tree_item_xml.LoadXml(current_tree_item.ProduceXml());


							System.Console.WriteLine("Removing PDO outputs");

							// Removes all the Sm attributes for the outputs
							System.Xml.XmlNodeList rxpdo_node_list = current_tree_item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo");
							foreach (System.Xml.XmlElement rxpdo_element in rxpdo_node_list) {
								rxpdo_element.RemoveAttribute("Sm");
							}


							System.Console.WriteLine("Removing PDO inputs");

							// Removes all the Sm attributes for the inputs
							System.Xml.XmlNodeList txpdo_node_list = current_tree_item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo");
							foreach (System.Xml.XmlElement txpdo_element in txpdo_node_list) {
								txpdo_element.RemoveAttribute("Sm");
							}


							System.Console.WriteLine("Setting PDO outputs");

							// Sets the Sm attribute for the outputs
							// The 'Outputs' node is required by the schema
							System.String[] pdo_output_list = modules_module_modules_module_pdo_node.SelectSingleNode(@"x:Outputs", namespace_manager).InnerText.Split(',');
							foreach (System.String pdo_output in pdo_output_list) {
								string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo[Index = ""{0}""]", pdo_output.Trim());
								((System.Xml.XmlElement) current_tree_item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "2");
							}


							System.Console.WriteLine("Setting PDO inputs");

							// Sets the Sm attribute for the inputs
							// The 'Inputs' node is required by the schema
							System.String[] pdo_input_list = modules_module_modules_module_pdo_node.SelectSingleNode(@"x:Inputs", namespace_manager).InnerText.Split(',');
							foreach (System.String pdo_input in pdo_input_list) {
								string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo[Index = ""{0}""]", pdo_input.Trim());
								((System.Xml.XmlElement) current_tree_item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "3");
							}


							// Throws an exception only on malformed xml
							current_tree_item.ConsumeXml(current_tree_item_xml.OuterXml);
						}


						// InitCmds

						// The 'InitCmds' node is not required by the schema
						System.Xml.XmlNode modules_module_modules_module_initcmds_node = modules_module_modules_module_node.SelectSingleNode(@"x:InitCmds", namespace_manager);
						if (modules_module_modules_module_initcmds_node != null) {
							System.Console.WriteLine("InitCmds");

							string modules_module_modules_module_initcmds_outerxml = modules_module_modules_module_initcmds_node.OuterXml;
							System.Console.WriteLine(modules_module_modules_module_initcmds_outerxml);

							consume_xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Mailbox><CoE>{0}</CoE></Mailbox></Slave></EtherCAT></TreeItem>", modules_module_modules_module_initcmds_outerxml);

							// Throws an exception only on malformed xml
							current_tree_item.ConsumeXml(consume_xml);
						}
					}
					System.Console.WriteLine("");
				}
			}


			// Saves the project
			project.Save();


			// Saves the solution
			// solution.Save() does not exist
			solution.SaveAs(solution_file_path);

		}
		catch (System.Exception e) {
			System.Console.WriteLine(e.Message);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();

		return 0;
	}
}
