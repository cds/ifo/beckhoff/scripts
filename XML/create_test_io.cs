// $Header: https://redoubt.ligo-wa.caltech.edu/svn/slowcontrols/trunk/TwinCAT3/Vacuum/LHO/Source/h0vacex_create_target.cs 3649 2018-01-22 19:17:46Z patrick.thomas@LIGO.ORG $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

// Test of the creation and configuration of IO terminals.

using System;
using System.Linq;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Text.RegularExpressions;
using TCatSysManagerLib;

class Program {
	static void set_Disabled(TCatSysManagerLib.ITcSmTreeItem item) {
		item.Disabled = DISABLED_STATE.SMDS_DISABLED;
	}

	static void set_PreviousPort(TCatSysManagerLib.ITcSmTreeItem item, string Port, string PhysAddr) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><PreviousPort Selected=""true""><Port>{0}</Port><PhysAddr>{1}</PhysAddr></PreviousPort></Slave></EtherCAT></TreeItem>", Port, PhysAddr);
		item.ConsumeXml(xml);
	}

	static void set_SyncUnit(TCatSysManagerLib.ITcSmTreeItem item, string SyncUnit) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><SyncUnits><SyncUnit>{0}</SyncUnit></SyncUnits></Slave></EtherCAT></TreeItem>", SyncUnit);
		item.ConsumeXml(xml);
	}

	static void set_PhysAddr(TCatSysManagerLib.ITcSmTreeItem item, string PhysAddr) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Info><PhysAddr>{0}</PhysAddr><PhysAddrFixed>true</PhysAddrFixed></Info></Slave></EtherCAT></TreeItem>", PhysAddr);
		item.ConsumeXml(xml);
	}

	static void set_InitCmd(TCatSysManagerLib.ITcSmTreeItem item, string Transition, string Comment, string Timeout, string Ccs, string Index, string SubIndex, string Data) {
		string xml = System.String.Format(@"<TreeItem><EtherCAT><Slave><Mailbox><CoE><InitCmds><InitCmd><Transition>{0}</Transition><Comment><![CDATA[{1}]]></Comment><Timeout>{2}</Timeout><Ccs>{3}</Ccs><Index>{4}</Index><SubIndex>{5}</SubIndex><Data>{6}</Data></InitCmd></InitCmds></CoE></Mailbox></Slave></EtherCAT></TreeItem>", Transition, Comment, Timeout, Ccs, Index, SubIndex, Data);
		item.ConsumeXml(xml);
	}

	static void set_ProcessData(TCatSysManagerLib.ITcSmTreeItem item, string[] pdo_input_list, string[] pdo_output_list) {
		System.Xml.XmlDocument item_xml = new System.Xml.XmlDocument();
		item_xml.LoadXml(item.ProduceXml());

		// Removes all of the Sm attributes for the outputs.
		System.Console.WriteLine("Removing PDO outputs");
		System.Xml.XmlNodeList rxpdo_node_list = item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo");
		foreach (System.Xml.XmlElement rxpdo_element in rxpdo_node_list) {
			rxpdo_element.RemoveAttribute("Sm");
		}

		// Removes all of the Sm attributes for the inputs.
		System.Console.WriteLine("Removing PDO inputs");
		System.Xml.XmlNodeList txpdo_node_list = item_xml.SelectNodes(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo");
		foreach (System.Xml.XmlElement txpdo_element in txpdo_node_list) {
			txpdo_element.RemoveAttribute("Sm");
		}

		// Sets the Sm attribute for the outputs.
		System.Console.WriteLine("Setting PDO outputs");
		foreach (string pdo_output in pdo_output_list) {
			System.Console.WriteLine(pdo_output);
			string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/RxPdo[Index = ""{0}""]", pdo_output);
			((System.Xml.XmlElement) item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "2");
		}

		// Sets the Sm attribute for the inputs.
		System.Console.WriteLine("Setting PDO inputs");
		foreach (string pdo_input in pdo_input_list) {
			System.Console.WriteLine(pdo_input);
			string xml_path = System.String.Format(@"TreeItem/EtherCAT/Slave/ProcessData/TxPdo[Index = ""{0}""]", pdo_input);
			((System.Xml.XmlElement) item_xml.SelectSingleNode(xml_path)).SetAttribute("Sm", "3");
		}

		item.ConsumeXml(item_xml.OuterXml);
	}


	[System.STAThread]
	static void Main(string[] args) {
		string solution_name = "TESTH1ECATC1";
		string solution_folder_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1";
		string solution_file_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1\TESTH1ECATC1.sln";

		string project_name = "TESTH1ECATC1";
		string project_folder_path = @"C:\SlowControls\TwinCAT3\Sandbox\Target\TESTH1ECATC1\TESTH1ECATC1";


		// Deletes the folder at the solution folder path if it exists.
		if (System.IO.Directory.Exists(solution_folder_path)) {
			System.IO.Directory.Delete(solution_folder_path, true);
		}

		// Deletes the folder at the project folder path if it exists.
		if (System.IO.Directory.Exists(project_folder_path)) {
			System.IO.Directory.Delete(project_folder_path, true);
		}

		// Creates folders at the solution and folder paths.
		System.IO.Directory.CreateDirectory(solution_folder_path);
		System.IO.Directory.CreateDirectory(project_folder_path);


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Create(solution_folder_path, solution_name);
			solution.SaveAs(solution_file_path);

			string template_path = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj";
			dynamic project = solution.AddFromTemplate(template_path, project_folder_path, project_name);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;


			// Creates the terminals.

			TCatSysManagerLib.ITcSmTreeItem parent;
			TCatSysManagerLib.ITcSmTreeItem child;

			System.Console.WriteLine("EtherCAT Master");
			parent = system_manager.LookupTreeItem("TIID");
			child = parent.CreateChild("EtherCAT Master", 111);

			System.Console.WriteLine("Term M0");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master");
			child = parent.CreateChild("Term M0 (EK1101)", 9099, "", "EK1101-0000-0017");
			set_PhysAddr(child, "5000");

			System.Console.WriteLine("Term M1");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)");
			child = parent.CreateChild("Term M1 (EL7342)", 9099, "", "EL7342-0000-0017");
			set_PhysAddr(child, "5005");
			string[] pdo_input_list = {"#x1a01","#x1a04","#x1a06","#x1a07","#x1a08","#x1a09","#x1a0b","#x1a0d"};
			string[] pdo_output_list = {"#x1601","#x1603","#x1604","#x1607","#x160b","#x160d"};
			set_ProcessData(child, pdo_input_list, pdo_output_list);

			System.Console.WriteLine("Term M2");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL7342)");
			child = parent.CreateChild("Term M2 (EL6002)", 9099, "", "EL6002-0000-0016");
			set_PhysAddr(child, "5010");

			System.Console.WriteLine("Term M3");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL6002)");
			child = parent.CreateChild("Term M3 (EL9011)", 9099, "", "EL9011");
			set_PhysAddr(child, "5015");

/*
			System.Console.WriteLine("Term M1");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)");
			child = parent.CreateChild("Term M1 (EL1004)", 9099, "", "EL1004-0000-0018");
			set_PhysAddr(child, "5005");
			set_Disabled(child);

			System.Console.WriteLine("Term M2");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)^Term M1 (EL1004)");
			child = parent.CreateChild("Term M2 (EL1004)", 9099, "", "EL1004-0000-0018");
			set_PhysAddr(child, "5010");
			set_SyncUnit(child, "SyncUnit1");

			System.Console.WriteLine("Term M3");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)^Term M2 (EL1004)");
			child = parent.CreateChild("Term M3 (EL1004)", 9099, "", "EL1004-0000-0018");
			set_PhysAddr(child, "5020");

			System.Console.WriteLine("Term M4");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)^Term M3 (EL1004)");
			child = parent.CreateChild("Term M4 (EL6002)", 9099, "", "EL6002-0000-0019");
			set_PhysAddr(child, "5110");
			set_InitCmd(child, "PS", "Baudrate", "0", "1", "32768", "17", "04");

			System.Console.WriteLine("Term M5");
			parent = system_manager.LookupTreeItem("TIID^EtherCAT Master^Term M0 (EK1101)");
			child = parent.CreateChild("Corner Chassis 2 L-3 (AB9000)", 9099, "", "V0000001b_P0000001b_R0001000d");
			set_PhysAddr(child, "8030");
			set_PreviousPort(child, "B", "5000");

			string[] pdo_input_list = {"#x1a02"};
			string[] pdo_output_list = {"#x1602"};
			set_ProcessData(child, pdo_input_list, pdo_output_list);
*/
/*
			// Creates the PLC project.
			TCatSysManagerLib.ITcSmTreeItem tipc = system_manager.LookupTreeItem("TIPC");
			tipc.CreateChild("PLC1", 0, "", @"Standard PLC Template.plcproj");


			// Adds a reference to the Vacuum library.
			TCatSysManagerLib.ITcSmTreeItem references = system_manager.LookupTreeItem("TIPC^PLC1^PLC1 Project^References");
			TCatSysManagerLib.ITcPlcLibraryManager library_manager = (TCatSysManagerLib.ITcPlcLibraryManager)references;
			library_manager.AddLibrary("Vacuum", "*", "LIGO");


			// Activates the configuration.
			// If this is not done a subsequent activate configuration will not create the links. It is not clear why.
			system_manager.ActivateConfiguration();
*/


			// Saves the project.
			project.Save();


			// Saves the solution.
			// solution.Save() does not exist.
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}
}

