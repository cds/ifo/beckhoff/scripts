// $Header: https://redoubt.ligo-wa.caltech.edu/svn/slowcontrols/trunk/TwinCAT3/Vacuum/LHO/Source/message_filter.cs 2912 2016-05-10 00:49:03Z patrick.thomas@LIGO.ORG $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

// Modified from: https://msdn.microsoft.com/en-us/library/ms228772.aspx

namespace EnvDteUtils {
	public class MessageFilter : IOleMessageFilter {
		// Class containing the IOleMessageFilter thread error-handling functions.

		// Start the filter.
		public static void Register() {
			IOleMessageFilter newFilter = new MessageFilter();
			IOleMessageFilter oldFilter = null;
			CoRegisterMessageFilter(newFilter, out oldFilter);
		}

		// Done with the filter, close it.
		public static void Revoke() {
			IOleMessageFilter oldFilter = null;
			CoRegisterMessageFilter(null, out oldFilter);
		}


		// IOleMessageFilter functions.

		// Handle incoming thread requests.
		int IOleMessageFilter.HandleInComingCall(int dwCallType, System.IntPtr hTaskCaller, int dwTickCount, System.IntPtr lpInterfaceInfo) {
			// Return the flag SERVERCALL_ISHANDLED.
			return 0;
		}

		// Thread call was rejected, so try again.
		int IOleMessageFilter.RetryRejectedCall(System.IntPtr hTaskCallee, int dwTickCount, int dwRejectType) {
			if (dwRejectType == 2) {
				// flag = SERVERCALL_RETRYLATER.

				// Retry the thread call immediately if return >= 0 & < 100.
				return 99;
			}
			// Too busy; cancel call.
			return -1;
		}

		int IOleMessageFilter.MessagePending(System.IntPtr hTaskCallee, int dwTickCount, int dwPendingType) {
			// Return the flag PENDINGMSG_WAITDEFPROCESS.
			return 2;
		}


		// Implements the IOleMessageFilter interface.
		[System.Runtime.InteropServices.DllImport("Ole32.dll")]
		private static extern int CoRegisterMessageFilter(IOleMessageFilter newFilter, out IOleMessageFilter oldFilter);
	}

	[System.Runtime.InteropServices.ComImport(), System.Runtime.InteropServices.Guid("00000016-0000-0000-C000-000000000046"), System.Runtime.InteropServices.InterfaceTypeAttribute(System.Runtime.InteropServices.ComInterfaceType.InterfaceIsIUnknown)]
	interface IOleMessageFilter {
		[System.Runtime.InteropServices.PreserveSig]
		int HandleInComingCall(int dwCallType, System.IntPtr hTaskCaller, int dwTickCount, System.IntPtr lpInterfaceInfo);

		[System.Runtime.InteropServices.PreserveSig]
		int RetryRejectedCall(System.IntPtr hTaskCallee, int dwTickCount, int dwRejectType);

		[System.Runtime.InteropServices.PreserveSig]
		int MessagePending(System.IntPtr hTaskCallee, int dwTickCount, int dwPendingType);
	}
}
