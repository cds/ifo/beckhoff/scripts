// $Header$

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

class Program {
	[System.STAThread]
	static void Main(string[] args) {
		string csv_file_path = args[0];
		string solution_file_path = args[1];


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.11.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Open(solution_file_path);

			dynamic project = solution.Projects.Item(1);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			TCatSysManagerLib.ITcSmTreeItem tipc = system_manager.LookupTreeItem("TIPC");
			clear_links(system_manager, tipc);

			using (Microsoft.VisualBasic.FileIO.TextFieldParser parser = new Microsoft.VisualBasic.FileIO.TextFieldParser(csv_file_path)) {
				parser.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
				parser.SetDelimiters(",");
				while (!parser.EndOfData) {
					string[] field_list = parser.ReadFields();

					if (field_list.Length == 2) {
						System.Console.WriteLine("{0}\n{1}\n", field_list[0], field_list[1]);

						string plc = field_list[0];
						string io = field_list[1];

						system_manager.LinkVariables(plc, io);
					}

					if (field_list.Length == 5) {
						System.Console.WriteLine("{0}\n{1}\n{2} {3} {4}\n", field_list[0], field_list[1], field_list[2], field_list[3], field_list[4]);

						string plc = field_list[0];
						string io = field_list[1];

						int offsA = System.Int32.Parse(field_list[2]);
						int offsB = System.Int32.Parse(field_list[3]);
						int size = System.Int32.Parse(field_list[4]);

						system_manager.LinkVariables(plc, io, offsA, offsB, size);
					}
				}
			}


			// Saves the project.
			project.Save();


			// Saves the solution.
			// solution.Save() does not exist.
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}

	static void clear_links(TCatSysManagerLib.ITcSysManager system_manager, TCatSysManagerLib.ITcSmTreeItem tree_item) {
		foreach (TCatSysManagerLib.ITcSmTreeItem child in tree_item) {
			if (child.ItemType == 7) {
				string path = child.PathName;
				System.Console.WriteLine("{0}", path);
				system_manager.UnlinkVariables(path);
			}

			clear_links(system_manager, child);
		}
	}

}
