﻿// $Header: https://redoubt.ligo-wa.caltech.edu/svn/slowcontrols/trunk/Scripts3/Common/read_links_from_csv.cs 3138 2016-09-07 04:46:29Z patrick.thomas@LIGO.ORG $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

using System.Linq;

class Program {
	[System.STAThread]
	static void Main(string[] args) {
		string solution_file_path = args[0];
		string csv_file_path = args[1];


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2)obj;

		System.Collections.Generic.List<string> line_list = new System.Collections.Generic.List<string>();

		try {
			dte.MainWindow.Activate();

			dynamic solution = dte.Solution;
			solution.Open(solution_file_path);

			dynamic project = solution.Projects.Item(1);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;

			// line_list.Add("PLC, IO, offsA, offsB, size");

			TCatSysManagerLib.ITcSmTreeItem root;

			root = system_manager.LookupTreeItem("TIPC^AuxCorner^AuxCorner Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^IscCorner^IscCorner Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^SqzCorner^SqzCorner Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^TcsCorner^TcsCorner Instance");
			line_list = line_list.Concat(read_links(root)).ToList();


			root = system_manager.LookupTreeItem("TIPC^AuxEndX^AuxEndX Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^IscEndX^IscEndX Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^TcsEndX^TcsEndX Instance");
			line_list = line_list.Concat(read_links(root)).ToList();


			root = system_manager.LookupTreeItem("TIPC^AuxEndY^AuxEndY Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^IscEndY^IscEndY Instance");
			line_list = line_list.Concat(read_links(root)).ToList();

			root = system_manager.LookupTreeItem("TIPC^TcsEndY^TcsEndY Instance");
			line_list = line_list.Concat(read_links(root)).ToList();


			System.IO.File.WriteAllLines(csv_file_path, line_list);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();
	}

	static System.Collections.Generic.List<string> read_links(TCatSysManagerLib.ITcSmTreeItem tree_item) {
		System.Collections.Generic.List<string> line_list = new System.Collections.Generic.List<string>();

		foreach (TCatSysManagerLib.ITcSmTreeItem child in tree_item) {
			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			doc.LoadXml(child.ProduceXml());

			System.Xml.XmlNodeList link_list = doc.SelectNodes(@"//LinkedWith");
			foreach (System.Xml.XmlNode link in link_list) {
				string plc = "\"" + child.PathName + "\"";
				string io = "\"" + link.InnerText + "\"";

				if (link.Attributes != null & link.Attributes["offsA"] != null && link.Attributes["offsB"] != null && link.Attributes["size"] != null) {
					string offsA = link.Attributes["offsA"].Value;
					string offsB = link.Attributes["offsB"].Value;
					string size = link.Attributes["size"].Value;
					string[] fields = {plc, io, offsA, offsB, size};
					string line = string.Join(", ", fields);
					line_list.Add(line);
				}
				else {
					string[] fields = {plc, io};
					string line = string.Join(", ", fields);
					line_list.Add(line);
				}
			}
			line_list = line_list.Concat(read_links(child)).ToList();
		}
		return line_list;
	}
}
