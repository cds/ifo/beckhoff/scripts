// $Header: $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford


class Program {
	[System.STAThread]
	static int Main(string[] args) {
		if (args.Length != 2) {
			System.Console.WriteLine("Invalid argument count");
			return -1;
		}

		string mapping_file_path = System.IO.Path.GetFullPath(args[0]);
		string solution_file_path = System.IO.Path.GetFullPath(args[1]);


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2) obj;

		try {
			dte.MainWindow.Activate();


			// Opens the solution
			dynamic solution = dte.Solution;
			solution.Open(solution_file_path);

			dynamic project = solution.Projects.Item(1);

			TCatSysManagerLib.ITcSysManager system_manager = project.Object;


			// Loads the mapping file
			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			doc.Load(mapping_file_path);


			// Adds the links

			System.Xml.XmlNodeList owner_a_node_list = doc.SelectNodes(@"/VarLinks/OwnerA");

			foreach (System.Xml.XmlNode owner_a_node in owner_a_node_list) {
				string owner_a_name = owner_a_node.Attributes["Name"].Value;
				System.Xml.XmlNodeList owner_b_node_list = owner_a_node.SelectNodes(@"./OwnerB");

				foreach (System.Xml.XmlNode owner_b_node in owner_b_node_list) {
					string owner_b_name = owner_b_node.Attributes["Name"].Value;
					System.Xml.XmlNodeList link_node_list = owner_b_node.SelectNodes(@"./Link");

					foreach (System.Xml.XmlNode link_node in link_node_list) {
						string var_a_relative_path = link_node.Attributes["VarA"].Value;
						string var_b_relative_path = link_node.Attributes["VarB"].Value;

						string var_a_path = owner_a_name + "^" + var_a_relative_path;
						string var_b_path = owner_b_name + "^" + var_b_relative_path;

						int offsA = 0;
						int offsB = 0;
						int size = 0;

						if (link_node.Attributes["OffsA"] != null) {
							offsA = int.Parse(link_node.Attributes["OffsA"].Value);
						}

						if (link_node.Attributes["OffsB"] != null) {
							offsB = int.Parse(link_node.Attributes["OffsB"].Value);
						}

						if (link_node.Attributes["size"] != null) {
							size = int.Parse(link_node.Attributes["size"].Value);
						}

						try {
							system_manager.LinkVariables(var_a_path, var_b_path, offsA, offsB, size);
						}
						catch (System.Exception e) {
							System.Console.WriteLine(e.Message);
						}
					}
				}
			}


			// Saves the project
			project.Save();


			// Saves the solution
			// solution.Save() does not exist
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine(e.Message);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();

		return 0;
	}
}
