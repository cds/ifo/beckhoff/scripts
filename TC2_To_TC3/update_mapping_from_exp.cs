// $Header: $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

using System.Linq;

class Program {
	[System.STAThread]
	static int Main(string[] args) {
		if (args.Length != 2) {
			System.Console.WriteLine("Expected 2 arguments");
			return -1;
		}


		string mapping_file_path = args[0];
		string new_mapping_file_path = args[1];

		string mapping_file_text = System.IO.File.ReadAllText(mapping_file_path);

		mapping_file_text = update_from_exp(@"C:\SlowControls\TwinCAT\Source\Current\Interferometer\Corner\Plc1.exp", mapping_file_text);
		mapping_file_text = update_from_exp(@"C:\SlowControls\TwinCAT\Source\Current\Interferometer\Corner\Plc2.exp", mapping_file_text);
		mapping_file_text = update_from_exp(@"C:\SlowControls\TwinCAT\Source\Current\Interferometer\Corner\Plc3.exp", mapping_file_text);
		mapping_file_text = update_from_exp(@"C:\SlowControls\TwinCAT\Source\Current\Interferometer\Corner\Plc4.exp", mapping_file_text);

		mapping_file_text = mapping_file_text.Replace(@"^PLC1", @"^PLC1^PLC1 Instance");
		mapping_file_text = mapping_file_text.Replace(@"^PLC2", @"^PLC2^PLC2 Instance");
		mapping_file_text = mapping_file_text.Replace(@"^PLC3", @"^PLC3^PLC3 Instance");
		mapping_file_text = mapping_file_text.Replace(@"^PLC4", @"^PLC4^PLC4 Instance");

		mapping_file_text = mapping_file_text.Replace(@"Standard^Inputs", @"Standard Inputs");
		mapping_file_text = mapping_file_text.Replace(@"Standard^Outputs", @"Standard Outputs");
		mapping_file_text = mapping_file_text.Replace(@"Fast^Inputs", @"Fast Inputs");
		mapping_file_text = mapping_file_text.Replace(@"Fast^Outputs", @"Fast Outputs");

		System.IO.File.WriteAllText(new_mapping_file_path, mapping_file_text);

		return 0;
	}

	static string update_from_exp(string exp_file_path, string mapping_file_text) {
		System.Collections.Generic.List<string> file_line_list = System.IO.File.ReadAllLines(exp_file_path).ToList();

		System.Text.RegularExpressions.Match match;

		string name = string.Empty;

		int state = 0;

		foreach (string line in file_line_list) {
			switch (state) {
				case 0:
					match = System.Text.RegularExpressions.Regex.Match(line, @"@GLOBAL_VARIABLE_LIST := '(.+)'");
					if (match.Success) {
						name = match.Groups[1].Value;
						state = 1;
					}

					break;

				case 1:
					match = System.Text.RegularExpressions.Regex.Match(line, @"^\s*(\w+)\s+AT");
					if (match.Success) {
						string old_name = match.Groups[1].Value;
						string new_name = System.String.Format("{0}.{1}", name, old_name);
						mapping_file_text = mapping_file_text.Replace(old_name, new_name);
					}

					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@OBJECT_END")) {
						state = 0;
					}

					break;
			}
		}

		return mapping_file_text;
	}
}
