// $Header: $

// Patrick J. Thomas, California Institute of Technology, LIGO Hanford

using System.Linq;
using System.Threading;

class Program {
	[System.STAThread]
	static int Main(string[] args) {
		if (args.Length != 7) {
			System.Console.WriteLine("Expected 7 arguments");
			return -1;
		}

		string exp_file_path = args[0];
		string solution_folder_path = args[1];
		string library_folder_path = args[2];
		string solution_name = args[3];
		string project_name = args[4];
		string plc_name = args[5];
		string enum_mapping_folder_path = args[6];


		if (System.IO.Directory.Exists(solution_folder_path)) {
			System.IO.Directory.Delete(solution_folder_path, true);
            Thread.Sleep(2000);
        }
		System.IO.Directory.CreateDirectory(solution_folder_path);

		string project_folder_path = solution_folder_path;


		EnvDteUtils.MessageFilter.Register();

		System.Type t = System.Type.GetTypeFromProgID("VisualStudio.DTE.15.0", true);
		object obj = System.Activator.CreateInstance(t, true);
		EnvDTE80.DTE2 dte = (EnvDTE80.DTE2) obj;

		try {
			dte.MainWindow.Activate();


			// Creates the solution.
			dynamic solution = dte.Solution;
			solution.Create(solution_folder_path, solution_name);


			// Saves the solution.
			string solution_file_path = System.IO.Path.ChangeExtension(System.IO.Path.Combine(solution_folder_path, solution_name), ".sln");
			solution.SaveAs(solution_file_path);


			// Creates the project.
			string template_path = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj";
			dynamic project = solution.AddFromTemplate(template_path, project_folder_path, project_name);


			// Creates the PLC project.
			TCatSysManagerLib.ITcSmTreeItem plcProject = create_plc(project, plc_name, exp_file_path, enum_mapping_folder_path);


			// Saves the PLC project as a library and installs it.
			string library_name = plc_name;
			TCatSysManagerLib.ITcPlcIECProject iecProject = (TCatSysManagerLib.ITcPlcIECProject) plcProject;
			string library_file_path = System.IO.Path.ChangeExtension(System.IO.Path.Combine(library_folder_path, library_name), ".library");
			iecProject.SaveAsLibrary(library_file_path, true);


			// Saves the project.
			project.Save();


			// Saves the solution.
			// solution.Save() does not exist.
			solution.SaveAs(solution_file_path);
		}
		catch (System.Exception e) {
			System.Console.WriteLine("{0}", e);
		}

		dte.Quit();
		System.Runtime.InteropServices.Marshal.ReleaseComObject(dte);

		EnvDteUtils.MessageFilter.Revoke();

		return 0;
	}

	static TCatSysManagerLib.ITcSmTreeItem create_plc(dynamic project, string plc_name, string exp_file_path, string enum_mapping_folder_path) {
		TCatSysManagerLib.ITcSysManager system_manager = project.Object;

		// Creates the PLC project
		TCatSysManagerLib.ITcSmTreeItem tipc = system_manager.LookupTreeItem("TIPC");
		tipc.CreateChild(plc_name, 0, "", @"Empty PLC Template.plcproj");

		string plc_project_path = System.String.Format("TIPC^{0}^{0} Project", plc_name);
		TCatSysManagerLib.ITcSmTreeItem plcProject = system_manager.LookupTreeItem(plc_project_path);


		// Sets the Company, Title and Version of the PLC project
		string xml = System.String.Format("<TreeItem><IECProjectDef><ProjectInfo><Company>LIGO</Company><Title>{0}</Title><Version>1.0</Version></ProjectInfo></IECProjectDef></TreeItem>", plc_name);
		plcProject.ConsumeXml(xml);


		// Creates the POUs folder
		plcProject.CreateChild("POUs", 601);

		// Creates the DUTs folder
		plcProject.CreateChild("DUTs", 601);

		// Creates the GVLs folder
		plcProject.CreateChild("GVLs", 601);


		string pous_folder_path = System.String.Format("{0}^POUs", plc_project_path);
		TCatSysManagerLib.ITcSmTreeItem plc_POUs = system_manager.LookupTreeItem(pous_folder_path);

		string duts_folder_path = System.String.Format("{0}^DUTs", plc_project_path);
		TCatSysManagerLib.ITcSmTreeItem plc_DUTs = system_manager.LookupTreeItem(duts_folder_path);

		string gvls_folder_path = System.String.Format("{0}^GVLs", plc_project_path);
		TCatSysManagerLib.ITcSmTreeItem plc_GVLs = system_manager.LookupTreeItem(gvls_folder_path);

		string references_path = System.String.Format("{0}^References", plc_project_path);
		TCatSysManagerLib.ITcSmTreeItem references = system_manager.LookupTreeItem(references_path);
		TCatSysManagerLib.ITcPlcLibraryManager library_manager = (TCatSysManagerLib.ITcPlcLibraryManager) references;


		populate_plc(plc_POUs, plc_DUTs, plc_GVLs, library_manager, exp_file_path, plc_name, enum_mapping_folder_path);


		return plcProject;
	}

	static void populate_plc(TCatSysManagerLib.ITcSmTreeItem plc_POUs, TCatSysManagerLib.ITcSmTreeItem plc_DUTs, TCatSysManagerLib.ITcSmTreeItem plc_GVLs, TCatSysManagerLib.ITcPlcLibraryManager library_manager, string exp_file_path, string plc_name, string enum_mapping_folder_path) {
		System.Collections.Generic.List<string> line_list = System.IO.File.ReadAllLines(exp_file_path).ToList();

		line_list = add_subtract_lines(line_list);

		line_list = convert_arrays(line_list);

		System.Collections.Generic.List<string[]> program_attributes_list = parse_programs(line_list);
		System.Collections.Generic.List<string[]> function_attributes_list = parse_functions(line_list);
		System.Collections.Generic.List<string[]> function_block_attributes_list = parse_function_blocks(line_list);
		System.Collections.Generic.List<string[]> enum_attributes_list = parse_enums(line_list);
		System.Collections.Generic.List<string[]> struct_attributes_list = parse_structs(line_list);
		System.Collections.Generic.List<string[]> gvl_attributes_list = parse_gvls(line_list);
		System.Collections.Generic.List<string[]> libraries_attributes_list = parse_libraries(line_list);


		System.Collections.Generic.List<string> enum_mapping_list = get_enum_mapping_list(exp_file_path);

		string write_enum_mapping_file_name = System.String.Format("{0}_enum_mapping.txt", plc_name);
		string write_enum_mapping_file_path = System.IO.Path.Combine(enum_mapping_folder_path, write_enum_mapping_file_name);
		System.IO.File.WriteAllLines(write_enum_mapping_file_path, enum_mapping_list);


		string library_name = string.Empty;
		string library_company = string.Empty;
		string read_enum_mapping_file_name = string.Empty;
		string read_enum_mapping_file_path = string.Empty;

		foreach (string[] attributes in libraries_attributes_list) {
			library_name = attributes[0];
			library_company = attributes[2];

			if (library_company == "LIGO") {
				read_enum_mapping_file_name = System.String.Format("{0}_enum_mapping.txt", library_name);
				read_enum_mapping_file_path = System.IO.Path.Combine(enum_mapping_folder_path, read_enum_mapping_file_name);
				enum_mapping_list = enum_mapping_list.Concat(System.IO.File.ReadAllLines(read_enum_mapping_file_path).ToList()).ToList();
			}
		}


		char[] sep = new char[] {' '};
		string[] enum_mapping_array = new string[] {};

		string path = string.Empty;
		string name = string.Empty;
		string declaration_text = string.Empty;
		string implementation_text = string.Empty;

		TCatSysManagerLib.ITcSmTreeItem tmp;
		string[] delimeter_list = {@"\/"};
		string[] folder_list = new string[] {};

		foreach (string[] attribute in program_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];
			implementation_text = attribute[3];


			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_POUs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[2]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
				implementation_text = System.Text.RegularExpressions.Regex.Replace(implementation_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem pou = tmp.CreateChild(name, 602);

			TCatSysManagerLib.ITcPlcDeclaration pou_declaration = (TCatSysManagerLib.ITcPlcDeclaration) pou;
			pou_declaration.DeclarationText = declaration_text;

			TCatSysManagerLib.ITcPlcImplementation pou_implementation = (TCatSysManagerLib.ITcPlcImplementation) pou;
			pou_implementation.ImplementationText = implementation_text;
		}


		foreach (string[] attribute in function_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];
			implementation_text = attribute[3];

			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_POUs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[2]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
				implementation_text = System.Text.RegularExpressions.Regex.Replace(implementation_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem pou = tmp.CreateChild(name, 603);

			TCatSysManagerLib.ITcPlcDeclaration pou_declaration = (TCatSysManagerLib.ITcPlcDeclaration) pou;
			pou_declaration.DeclarationText = declaration_text;

			TCatSysManagerLib.ITcPlcImplementation pou_implementation = (TCatSysManagerLib.ITcPlcImplementation) pou;
			pou_implementation.ImplementationText = implementation_text;
		}


		foreach (string[] attribute in function_block_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];
			implementation_text = attribute[3];

			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_POUs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[2]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
				implementation_text = System.Text.RegularExpressions.Regex.Replace(implementation_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem pou = tmp.CreateChild(name, 604);

			TCatSysManagerLib.ITcPlcDeclaration pou_declaration = (TCatSysManagerLib.ITcPlcDeclaration) pou;
			pou_declaration.DeclarationText = declaration_text;

			TCatSysManagerLib.ITcPlcImplementation pou_implementation = (TCatSysManagerLib.ITcPlcImplementation) pou;
			pou_implementation.ImplementationText = implementation_text;
		}


		foreach (string[] attribute in enum_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];

			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_DUTs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[1]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem dut = tmp.CreateChild(name, 605);

			TCatSysManagerLib.ITcPlcDeclaration dut_declaration = (TCatSysManagerLib.ITcPlcDeclaration) dut;
			dut_declaration.DeclarationText = declaration_text;
		}


		foreach (string[] attribute in struct_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];

			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_DUTs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[2]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem dut = tmp.CreateChild(name, 606);

			TCatSysManagerLib.ITcPlcDeclaration dut_declaration = (TCatSysManagerLib.ITcPlcDeclaration) dut;
			dut_declaration.DeclarationText = declaration_text;
		}


		foreach (string[] attribute in gvl_attributes_list) {
			path = attribute[0];
			name = attribute[1];
			declaration_text = attribute[2];

			folder_list = path.Split(delimeter_list, System.StringSplitOptions.RemoveEmptyEntries);
			tmp = plc_GVLs;
			foreach (string folder in folder_list) {
				try {
					tmp = tmp.LookupChild(folder);
				}
				catch (System.Exception) {
					tmp = tmp.CreateChild(folder, 601);
				}
			}

			foreach (string enum_mapping in enum_mapping_list) {
				enum_mapping_array = enum_mapping.Split(sep, System.StringSplitOptions.RemoveEmptyEntries);
				string pattern = System.String.Format(@"([\W-[\.]]){0}([\W-[\.]])", enum_mapping_array[0]);
				string replacement = System.String.Format("$1{0}$2", enum_mapping_array[2]);
				declaration_text = System.Text.RegularExpressions.Regex.Replace(declaration_text, pattern, replacement);
			}

			TCatSysManagerLib.ITcSmTreeItem gvl = tmp.CreateChild(name, 615);

			TCatSysManagerLib.ITcPlcDeclaration gvl_declaration = (TCatSysManagerLib.ITcPlcDeclaration) gvl;
			gvl_declaration.DeclarationText = declaration_text;
		}


		foreach (string[] attribute in libraries_attributes_list) {
			library_manager.AddLibrary(attribute[0], attribute[1], attribute[2]);
		}
	}


	static System.Collections.Generic.List<string> add_subtract_lines(System.Collections.Generic.List<string> line_list) {
		System.Collections.Generic.List<string> modified_line_list = new System.Collections.Generic.List<string>();

		bool skip_line = false;
		bool tc3_subtract = false;

		foreach (string line in line_list) {
			skip_line = false;

			if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^\(\*TC3-\*\)$")) {
				skip_line = true;

				if (tc3_subtract == false) {
					tc3_subtract = true;
				}
				else {
					tc3_subtract = false;
				}
			}

			if ( System.Text.RegularExpressions.Regex.IsMatch(line, @"^\(\*TC3\+$") || System.Text.RegularExpressions.Regex.IsMatch(line, @"^TC3\+\*\)$") ) {
				skip_line = true;
			}

			if (skip_line == false && tc3_subtract == false) {
				modified_line_list.Add(line);
			}
		}

		return modified_line_list;
	}


	static System.Collections.Generic.List<string> convert_arrays(System.Collections.Generic.List<string> line_list) {
		string modified_line = string.Empty;
		System.Collections.Generic.List<string> modified_line_list = new System.Collections.Generic.List<string>();

		foreach (string line in line_list) {
			modified_line = line.Replace("(*TC3[*)", "[").Replace("(*TC3]*)", "]");
			modified_line_list.Add(modified_line);
		}

		return modified_line_list;
	}


	static System.Collections.Generic.List<string[]> parse_programs(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();
		System.Collections.Generic.List<string> implementation_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;
		string implementation_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					declaration_line_list.Clear();
					implementation_line_list.Clear();

					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"^PROGRAM (.+)$");
					if (match.Success) {
						name = match.Groups[1].Value;

						declaration_line_list.Add(line);

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 2;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^END_PROGRAM$")) {
						state = 3;
					}
					else {
						implementation_line_list.Add(line);
					}

					break;

				case 3:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());
					implementation_text = System.String.Join("\n", implementation_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text, implementation_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_functions(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();
		System.Collections.Generic.List<string> implementation_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;
		string implementation_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					declaration_line_list.Clear();
					implementation_line_list.Clear();

					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"^FUNCTION (\S+)\s*:");
					if (match.Success) {
						name = match.Groups[1].Value;

						declaration_line_list.Add(line);

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 2;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^END_FUNCTION$")) {
						state = 3;
					}
					else {
						implementation_line_list.Add(line);
					}

					break;

				case 3:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());
					implementation_text = System.String.Join("\n", implementation_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text, implementation_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_function_blocks(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();
		System.Collections.Generic.List<string> implementation_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;
		string implementation_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					declaration_line_list.Clear();
					implementation_line_list.Clear();

					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"^FUNCTION_BLOCK (.+)$");
					if (match.Success) {
						name = match.Groups[1].Value;

						declaration_line_list.Add(line);

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 2;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^END_FUNCTION_BLOCK$")) {
						state = 3;
					}
					else {
						implementation_line_list.Add(line);
					}

					break;

				case 3:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());
					implementation_text = System.String.Join("\n", implementation_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text, implementation_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_enums(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					declaration_line_list.Clear();

					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"^TYPE (\S+)\s*:");
					if (match.Success) {
						name = match.Groups[1].Value;

						declaration_line_list.Add(line);

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^STRUCT$")) {
						state = 0;
					}
					else {
						declaration_line_list.Add(line);
						state = 2;
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 3;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 3:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_structs(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					declaration_line_list.Clear();

					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"^TYPE (\S+)\s*:");
					if (match.Success) {
						name = match.Groups[1].Value;

						declaration_line_list.Add(line);

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^STRUCT$")) {
						declaration_line_list.Add(line);

						state = 2;
					}
					else {
						state = 0;
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 3;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 3:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_gvls(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string path = string.Empty;

		string name = string.Empty;

		System.Collections.Generic.List<string> declaration_line_list = new System.Collections.Generic.List<string>();

		string declaration_text = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					match = System.Text.RegularExpressions.Regex.Match(line, @"@PATH := \'(.*)\'");
					if (match.Success) {
						path = match.Groups[1].Value;
					}

					match = System.Text.RegularExpressions.Regex.Match(line, @"@GLOBAL_VARIABLE_LIST := '(.+)'");
					if (match.Success) {
						name = match.Groups[1].Value;
					}

					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^VAR_GLOBAL")) {
						declaration_line_list.Clear();

						declaration_line_list.Add(line);

						state = 1;
					}

					break;


				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@OBJECT_END")) {
						state = 2;
					}
					else {
						declaration_line_list.Add(line);
					}

					break;

				case 2:
					declaration_text = System.String.Join("\n", declaration_line_list.ToArray());

					attributes = new string[] { path, name, declaration_text };
					attributes_list.Add(attributes);

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string[]> parse_libraries(System.Collections.Generic.List<string> line_list) {
		int state = 0;

		System.Text.RegularExpressions.Match match;

		string name = string.Empty;

		string[] attributes;

		System.Collections.Generic.List<string[]> attributes_list = new System.Collections.Generic.List<string[]>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^LIBRARY$")) {
						state = 1;
					}

					break;

				case 1:
					match = System.Text.RegularExpressions.Regex.Match(line, @"^(\S+)\s");
					if (match.Success) {
						name = match.Groups[1].Value;
						name = System.IO.Path.GetFileNameWithoutExtension(name);

						if (name == "STANDARD") {
							attributes = new string[] { "Tc2_Standard", "3.3.2.0", "Beckhoff Automation GmbH" };
							attributes_list.Add(attributes);
						}
						else if (name == "TcUtilities") {
							attributes = new string[] { "Tc2_Utilities", "3.3.36.0", "Beckhoff Automation GmbH" };
							attributes_list.Add(attributes);
						}
						else if (name == "TcSystem") {
							attributes = new string[] { "Tc2_System", "3.4.22.0", "Beckhoff Automation GmbH" };
							attributes_list.Add(attributes);
						}
						else if (name == "TcEtherCAT") {
							attributes = new string[] { "Tc2_EtherCAT", "3.3.13.0", "Beckhoff Automation GmbH" };
							attributes_list.Add(attributes);
						}
						else if (name == "TcBase") {
							// attributes = new string[] { "Tc2_System", "3.4.17.0", "Beckhoff Automation GmbH" };
							// attributes_list.Add(attributes);
						}
						else if (name == "COMlibV2") {
							attributes = new string[] { "Tc2_SerialCom", "3.3.6.0", "Beckhoff Automation GmbH" };
							attributes_list.Add(attributes);
						}
						else {
							attributes = new string[] { name, "1.0", "LIGO" };
							attributes_list.Add(attributes);
						}
					}

					state = 0;

					break;
			}
		}

		return attributes_list;
	}


	static System.Collections.Generic.List<string> get_enum_mapping_list(string exp_file_path) {
		System.Collections.Generic.List<string> line_list = System.IO.File.ReadAllLines(exp_file_path).ToList();

		int state = 0;

		System.Text.RegularExpressions.Match match;

		string type_name = string.Empty;
		string original_name = string.Empty;
		string comment_name = string.Empty;
		string qualified_name = string.Empty;

		string enum_mapping = string.Empty;
		System.Collections.Generic.List<string> enum_mapping_list = new System.Collections.Generic.List<string>();

		foreach (string line in line_list) {
			switch (state) {
				case 0:
					match = System.Text.RegularExpressions.Regex.Match(line, @"^TYPE (\S+)\s*:");
					if (match.Success) {
						type_name = match.Groups[1].Value;

						state = 1;
					}

					break;

				case 1:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"^STRUCT$")) {
						state = 0;
					}
					else {
						match = System.Text.RegularExpressions.Regex.Match(line, @"^\s*([0-9a-zA-Z_]+).*\(\*\s*(\S+)\s*\*\)$");
						if (match.Success) {
							original_name = match.Groups[1].Value;
							comment_name = match.Groups[2].Value;
							qualified_name = System.String.Format("{0}.{1}", type_name, comment_name);

							enum_mapping = System.String.Join(" ", original_name, comment_name, qualified_name);
							enum_mapping_list.Add(enum_mapping);
						}

						state = 2;
					}

					break;

				case 2:
					if (System.Text.RegularExpressions.Regex.IsMatch(line, @"@END_DECLARATION")) {
						state = 0;
					}
					else {
						match = System.Text.RegularExpressions.Regex.Match(line, @"^\s*([0-9a-zA-Z_]+).*\(\*\s*(\S+)\s*\*\)$");
						if (match.Success) {
							original_name = match.Groups[1].Value;
							comment_name = match.Groups[2].Value;
							qualified_name = System.String.Format("{0}.{1}", type_name, comment_name);

							enum_mapping = System.String.Join(" ", original_name, comment_name, qualified_name);
							enum_mapping_list.Add(enum_mapping);
						}
					}

					break;
			}
		}

		return enum_mapping_list;
	}

}
